<?php

namespace MiamiOH\RestngClassroom\Services;



class Classroom extends \MiamiOH\RESTng\Service
{

    private $dataSource = '';
    private $database = '';
    private $configuration = '';
    private $datasource_name = 'MUWS_GEN_PROD'; // secure datasource


    /************************************************/
    /**********Setter Dependency Injection***********/
    /***********************************************/

    // Inject the datasource object provided by the framework
    public function setDataSource($datasource)
    {
        $this->dataSource = $datasource;
    }

    // Inject the database object provided by the framework
    public function setDatabase($database)
    {
        $this->database = $database->getHandle($this->datasource_name);
    }

    // Inject the configuration object provided by the framework
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    //Called when the building code, room number and termid are supplied as parameters
    public function getClassroom() {
        //log
        $this->log->debug('  Classroom:getClassroom() was called.');
        //initial variable setup
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        // Get paramaters entered in by user
        $building = strtoupper($request->getResourceParam('buildingCode'));
        $roomNumber = strtoupper($request->getResourceParam('roomNumber'));
        $termId = $request->getResourceParam('termId');
        // If the inputs aren't properly formatted, throw a 404.
        if (preg_match("/^[A-Z0-9]\w{2,5}$/", $building) !== 1) { //3-6 alphanumeric characters
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }
        if (preg_match("/^[A-Z0-9]*$/", $roomNumber) !== 1) { //must be alphanuneric
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }
        if (preg_match("/^\d{6}$/", $termId) !== 1) { //must be 6 digits
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        // build where clause for query based on if multiple termids have been entered
        $whereString = $this->buildWhereString($termId, false);

        $payload = array();
        $payload_course = array();
        $payload_instructor = array();

        $results = $this->database->queryall_array($this->buildQuery($whereString), $building, $roomNumber);

        foreach ($results as $row) {
            $payload[] = $this->buildRecord($row);
            $payload_course[] = $this->buildRecordCourse($row);
            $payload_instructor[] = $this->buildRecordInstructor($row);
        }

        // remove duplicate terms from the payload
        $payload = $this->cleanPayload($payload);
        $payload_course = $this->cleanPayload($payload_course);
        $payload_instructor = $this->cleanPayload($payload_instructor);

        // push the courses to the main payload array as there is only one combination
        // of building, room number, and term
        $payload['courses'] = $payload_course;

        // for every course, match teachers based on crn to it
        for ($i = 0; $i < sizeof($payload['courses']); $i++) {
            for ($x = 0; $x < sizeof($payload_instructor); $x++) {
                if ($payload_instructor[$x]['assignmentCrn'] == $payload['courses'][$i]['courseCrn']) {
                    $payload['courses'][$i]['instructors'][] = $payload_instructor[$x];
                }
            }
        }

        // If payload has no length return not found
        if (sizeof($payload) <= 0) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    public function getClassrooms() {
        //log
        $this->log->debug('  Classroom:getClassrooms() was called.');

        //initial variable setup
        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();
        $offset = $request->getOffset();
        $limit = $request->getLimit();

        //retrieve the inputs
        $building = strtoupper($request->getResourceParam('buildingCode'));
        $roomNumber = strtoupper($request->getResourceParam('roomNumber'));
        $termIds = $options['termIds'];
        //verify the inputs are in an acceptable format. if not, 404.
        if (preg_match("/^[A-Z0-9]\w{2,5}$/", $building) !== 1) { //3-6 alphanumeric characters
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }
        if (preg_match("/^[A-Z0-9]*$/", $roomNumber) !== 1) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }
        $multiple=true;
        if (!empty($termIds)) {
            foreach ($termIds as $term) {
                if(count($termIds)>1) {
                    $term = preg_replace("/[^0-9,.]/", "", $term);
                    if (preg_match("/^\d{6}$/", $term) !== 1) { //must be 6 digits
                        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                        $response->setPayload([$term]);
                        return $response;
                    }
                } else {
                    $termIds = preg_replace("/[^0-9,.]/", "", $term);
                    $multiple=false;
                    if (preg_match("/^\d{6}$/", $termIds) !== 1) { //must be 6 digits
                        $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
                        $response->setPayload([$termIds]);
                        return $response;
                    }
                }
            }
        }

        $whereString = "";

        // check and see if the user has given any termids
        if (isset($options['termIds'])) {
            $whereString = $this->buildWhereString($termIds, $multiple);
        }
        //call the DB
        $results = $this->database->queryall_array($this->buildQuery($whereString), $building, $roomNumber);

        //make our payload arrays
        $payload = [];
        $payload_course = [];
        $payload_instructor = [];


        //spin through the results and separate courses and instructors
        foreach ($results as $row) {
            $payload[] = $this->buildRecord($row);
            $payload_course[] = $this->buildRecordCourse($row);
            $payload_instructor[] = $this->buildRecordInstructor($row);
        }


        // remove duplicates from the payload arrays
        $payload = $this->cleanPayload($payload);
        $payload_course = $this->cleanPayload($payload_course);
        $payload_instructor = $this->cleanPayload($payload_instructor);


        // build the payload!
        $count = 0;
        for ($p = 0; $p < sizeof($payload); $p++) {
            for ($i = 0; $i < sizeof($payload_course); $i++) {
                if ($payload_course[$i]['termId'] == $payload[$p]['termId']) {
                    $payload[$p]['courses'][$count++] = $payload_course[$i];
                    for ($x = 0; $x < sizeof($payload_instructor); $x++) {
                        if (($payload_instructor[$x]['assignmentCrn'] === $payload_course[$i]['courseCrn'])) {
                            $payload[$p]['courses'][$count - 1]['instructors'][] = $payload_instructor[$x];

                        }

                    }

                }


            }
            $count = 0;
        }


        // Response was successful and Return information
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        $response->setPayload($payload);
        return $response;
    }

    private function cleanPayload($payload)
    {
        $clean_payload = array();
        for ($i = 0; $i < sizeof($payload); $i++) {
            if (!in_array($payload[$i], $clean_payload)) {
                $clean_payload[] = $payload[$i];
            }
        }
        return $clean_payload;
    }

    private function buildRecord($row)
    {
        $record = array(
            'buildingCode' => $row['ssrmeet_bldg_code'],
            'roomNumber' => $row['ssrmeet_room_code'],
            'termId' => $row['ssrmeet_term_code'],
        );

        return $record;
    }

    private function buildRecordCourse($row)
    {
        $record = array(
            'subjCode' => $row['ssbsect_subj_code'],
            'courseNumber' => $row['ssbsect_crse_numb'],
            'courseTitle' => $row['ssbsect_crse_title'],
            'courseCrn' => $row['ssbsect_crn'],
            'campusCode' => $row['ssbsect_camp_code'],
            'startTime' => $row['begin_time'],
            'endTime' => $row['end_time'],
            'days' => $row['days'],
            'termId' => $row['ssrmeet_term_code'],

        );
        return $record;
    }

    private function buildRecordInstructor($row)
    {
        $record = array(
            'uniqueId' => $row['szbuniq_unique_id'],
            'pidm' => $row['szbuniq_pidm'],
            'assignmentCrn' => $row['sirasgn_crn'],
        );
        return $record;
    }

    private function buildQuery($whereString)
    {
        return "select distinct
            ssrmeet_bldg_code, ssrmeet_room_code, ssrmeet_term_code, ssbsect_subj_code, 
            ssbsect_crse_numb, ssbsect_crse_title, ssbsect_crn, ssbsect_camp_code,
            to_char(to_date(ssrmeet_begin_time,'hh24mi'),'hh24:mi')  begin_time,
            to_char(to_date(ssrmeet_end_time,'hh24mi'),'hh24:mi')  end_time,
            ssrmeet_sun_day || ssrmeet_mon_day || ssrmeet_tue_day || ssrmeet_wed_day || ssrmeet_thu_day || ssrmeet_fri_day ||ssrmeet_sat_day days,
            szbuniq_unique_id, szbuniq_pidm, sirasgn_crn
            from ssrmeet, ssbsect, szbuniq, sirasgn
            where ssrmeet_bldg_code = ?
            and ssrmeet_room_code = ? " .
            $whereString
            . " and ssbsect_camp_code = 'O'
            and ssbsect_crn = ssrmeet_crn
            and sirasgn_crn = ssrmeet_crn
            and sirasgn_term_code = ssrmeet_term_code
            and szbuniq_pidm = sirasgn_pidm 
            and ssbsect_term_code = ssrmeet_term_code
            order by ssrmeet_bldg_code, ssrmeet_room_code, ssrmeet_term_code, ssbsect_subj_code, ssbsect_crse_numb, 
            ssbsect_crse_title, ssbsect_crn, begin_time, end_time, days, szbuniq_unique_id, szbuniq_pidm";
    }

    private function buildWhereString($info, $multiple)
    {
        $whereString = "";
        if ($multiple) {

            foreach ($info as $data) {
                if (strlen($whereString) > 0) {
                    $data=preg_replace("/[^0-9,.]/", "", $data);
                    $whereString .= "or ssrmeet_term_code = '" . $data . "' ";
                } else {
                    $data=preg_replace("/[^0-9,.]/", "", $data);
                    $whereString .= "and (ssrmeet_term_code = '" . $data . "' ";
                }
            }
            $whereString .= ")";
        } else {
            $info=preg_replace("/[^0-9,.]/", "", $info);
            $whereString = "and ssrmeet_term_code = " . $info . " ";
        }
        return $whereString;
    }
}
