<?php

namespace MiamiOH\RestngClassroom\Resources;

use MiamiOH\RESTng\App;

class ClassroomResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{

    private $tag = "Classroom";
    private $dot_path = "Classroom";
    private $s_path = "/classroom/v1";
    private $bs_path = 'MiamiOH\RestngClassroom\Services\Classroom';

    public function registerDefinitions(): void
    {

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Classroom',
            'type' => 'object',
            'properties' => array(
                'buildingCode' => array(
                    'type' => 'string',
                    'description' => '3 letter code of building.'
                ),
                'roomNumber' => array(
                    'type' => 'string',
                    'description' => 'Number of room within a building.'
                ),
                'termId' => array(
                    'type' => 'string',
                    'description' => '6 number Id for the semester.'
                ),
                'courses' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classroom.Courses'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Classrooms',
            'type' => 'object',
            'properties' => array(
                'classrooms' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classroom'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Classroom.Courses',
            'type' => 'object',
            'properties' => array(
                'subjCode' => array(
                    'type' => 'string',
                    'description' => 'Three letter subject code'
                ),
                'crseNumb' => array(
                    'type' => 'string',
                    'description' => 'Number of the course'
                ),
                'crseTitle' => array(
                    'type' => 'string',
                    'description' => 'Title of the course'
                ),
                'crseCrn' => array(
                    'type' => 'string',
                    'description' => 'crn of the course'
                ),
                'startTime' => array(
                    'type' => 'string',
                    'description' => 'Time when course starts'
                ),
                'endTime' => array('type' =>
                    'string', 'description' => 'Time when course ends'
                ),
                'days' => array('type' => 'string',
                    'description' => 'Days when course meets'
                ),
                'instructor' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/' . $this->dot_path . 'Get.Return.Classroom.Instructor'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . 'Get.Return.Classroom.Instructor',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array(
                    'type' => 'string',
                    'description' => 'unique Id for instructor of course'
                ),
                'pidm' => array(
                    'type' => 'string',
                    'description' => 'pidm for instructor of course'
                ),
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Classroom.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classroom',
            )
        ));

        $this->addDefinition(array(
            'name' => $this->dot_path . '.Get.Return.Classrooms.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classrooms',
            )
        ));

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Classroom',
            'class' => $this->bs_path,
            'description' => 'This service provides resources about Classrooms.',
            'set' => array(
                'database' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                ),
                'configuration' => array(
                    'type' => 'service',
                    'name' => 'APIConfiguration'
                ),
                'dataSource' => array(
                    'type' => 'service',
                    'name' => 'APIDataSourceFactory'
                ),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.get.Room',
                'description' => 'Return a classroom',
                'pattern' => $this->s_path . '/:buildingCode/:roomNumber/:termId',
                'service' => 'Classroom',
                'method' => 'getClassroom',
                'returnType' => 'model',
                'params' => array(
                    'buildingCode' => array(
                        'description' => '3-6 character alphanumeric building code'
                    ),
                    'roomNumber' => array(
                        'description' => 'Number of room within a building.'
                    ),
                    'termId' => array(
                        'description' => '6 number Id for the semester.'
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of classrooms',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classroom.Collection',
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        ),
                    ),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'Classroom',
                            'key' => 'view'
                        ),
                    ),
                ),
            )
        );

        $this->addResource(array(
                'action' => 'read', //GET
                'name' => $this->dot_path . '.get.Rooms',
                'description' => 'Return a classroom',
                'pattern' => $this->s_path . '/:buildingCode/:roomNumber',
                'service' => 'Classroom',
                'method' => 'getClassrooms',
                'returnType' => 'model',
                'params' => array(
                    'buildingCode' => array(
                        'description' => '3-6 character alphanumeric building code'
                    ),
                    'roomNumber' => array(
                        'description' => 'Number of room within a building.'
                    ),
                ),
                'options' => array(
                    'termIds' => array(
                        'type' => 'list',
                        'description' => 'A list of term Ids.',
                    ),
                ),
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of classrooms',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->dot_path . '.Get.Return.Classrooms.Collection',
                        )
                    ),
                ),
                'middleware' => array(
                    'authenticate' => array(
                        array(
                            'type' => 'token'
                        ),
                    ),
                    'authorize' => array(
                        array(
                            'type' => 'authMan',
                            'application' => 'WebServices',
                            'module' => 'Classroom',
                            'key' => 'view'
                        ),
                    ),
                ),
            )
        );

    }

    public function registerOrmConnections(): void
    {

    }
}