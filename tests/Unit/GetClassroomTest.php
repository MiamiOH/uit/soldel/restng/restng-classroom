<?php

namespace MiamiOH\RestngClassroom\Tests\Unit;

class getClassroomTest extends \MiamiOH\RESTng\Testing\TestCase
{

    /*************************/
    /**********Set Up*********/
    /*************************/
    private $api, $user, $request, $dbh, $classroom;

    // set up method automatically called by PHPUnit before every test method:

    protected function setUp()
    {

        //set up the mock api:
        $this->api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'getResourceParam'))
            ->getMock();

        $this->api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions'))
            ->getMock();

        //set up the mock dbh:
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);

        $ds = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Datasource')
            ->disableOriginalConstructor()
            ->setMethods(array('getDataSource'))
            ->getMock();

        //set up the service with the mocked out resources:
        $this->classroom = new \MiamiOH\RestngClassroom\Services\Classroom();
        $this->classroom->setDatabase($db);
        $this->classroom->setDatasource($ds);
        $this->classroom->setApiUser($this->user);
        $this->classroom->setRequest($this->request);

    }

    /*************************/
    /**********Tests**********/
    /*************************/


    /*
     *   Tests Case where term Id is empty
     *   Actual: No term Id is specified for the classroom.
     *	 Expected Return: At least one term Id must be specified.
     */

    public function testRecordTermIdEmpty()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsTermIdEmpty')));

        $this->request->method('getResourceParam')
            ->will($this->onConsecutiveCalls('BEN', '001', ''));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryEmptyTermId')));

        $response = $this->classroom->getClassrooms();

        $this->assertEquals(404, $response->getStatus());

    }


    /**
     * Mock Database Return for Single Term Id
     */
    public function mockQueryEmptyTermId()
    {
        return [
            [
                'ssrmeet_bldg_code' => 'BEN',
                'ssrmeet_room_code' => '001',
                'ssrmeet_term_code' => '',
                'ssbsect_subj_code' => 'CSE',
                'ssbsect_crse_numb' => '148',
                'ssbsect_crse_title' => null,
                'ssbsect_crn' => '59387',
                'ssbsect_camp_code' => 'null',
                'begin_time' => '11:30',
                'end_time' => '12:50',
                'days' => 'MW',
                'szbuniq_unique_id' => 'MEANEYS2',
                'szbuniq_pidm' => '1430100',
                'sirasgn_crn' => '59387'

            ],
        ];
    }

    /**
     * Expected Return from Single Term Id
     */
    public function expectedEmptyTermIdResponse()
    {
        return [
            [
                'buildingCode' => 'BEN',
                'roomNumber' => '001',
                'termId' => '',
                'courses' =>
                    [
                        [
                            'subjCode' => 'CSE',
                            'courseNumber' => '148',
                            'courseTitle' => null,
                            'courseCrn' => '59387',
                            'campusCode' => 'null',
                            'startTime' => '11:30',
                            'endTime' => '12:50',
                            'days' => 'MW',
                            'termId' => '',
                            'instructors' =>
                                [
                                    ['uniqueId' => 'MEANEYS2',
                                        'pidm' => '1430100',
                                        'assignmentCrn' => '59387']
                                ],
                        ],
                    ],
            ],
        ];
    }

    /**
     * Mock Options for Empty Term Id
     */
    public function mockOptionsTermIdEmpty()
    {
        return [
            'termIds' => ['']
        ];
    }

    /*
     *   Test Case for single term Id
     *   Actual: Single term Id is specified for the classroom.
     *	 Expected Return: Single term Id is specified.
     */

    public function testRecordSingleTermId()
    {
        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSingleTermId')));

        $this->request->method('getResourceParam')
            ->will($this->onConsecutiveCalls('BEN', '001'));

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQuerySingleTermId')));

        $response = $this->classroom->getClassrooms();
        $this->assertEquals($this->expectedSingleTermIdResponse(), $response->getPayload());

    }

    /**
     * Mock Options for Single Term Id
     */
    public function mockOptionsSingleTermId()
    {
        return [
            'termIds' => ['201720']
        ];
    }

    /**
     * Mock Database Return for Single Term Id
     */
    public function mockQuerySingleTermId()
    {
        return [
            [
                'ssrmeet_bldg_code' => 'BEN',
                'ssrmeet_room_code' => '001',
                'ssrmeet_term_code' => '201720',
                'ssbsect_subj_code' => 'CSE',
                'ssbsect_crse_numb' => '148',
                'ssbsect_crse_title' => null,
                'ssbsect_crn' => '59387',
                'ssbsect_camp_code' => 'null',
                'begin_time' => '11:30',
                'end_time' => '12:50',
                'days' => 'MW',
                'szbuniq_unique_id' => 'MEANEYS2',
                'szbuniq_pidm' => '1430100',
                'sirasgn_crn' => '59387'

            ],
        ];
    }

    /**
     * Expected Return from Single Term Id
     */
    public function expectedSingleTermIdResponse()
    {
        return [
            [
                'buildingCode' => 'BEN',
                'roomNumber' => '001',
                'termId' => '201720',
                'courses' =>
                    [
                        [
                            'subjCode' => 'CSE',
                            'courseNumber' => '148',
                            'courseTitle' => null,
                            'courseCrn' => '59387',
                            'campusCode' => 'null',
                            'startTime' => '11:30',
                            'endTime' => '12:50',
                            'days' => 'MW',
                            'termId' => '201720',
                            'instructors' =>
                                [
                                    ['uniqueId' => 'MEANEYS2',
                                        'pidm' => '1430100',
                                        'assignmentCrn' => '59387']
                                ],
                        ],
                    ],
            ],
        ];
    }

    /*
     *   Test Case for multiple term Id
     *   Actual: Multiple term Id is specified.
     *	 Expected Return: Multiple term Id response.
     */

    public function testRecordMultipleTermId()
    {

        $this->request->expects($this->once())->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsMultipleTermId')));

        $this->request->method('getResourceParam')
            ->will($this->onConsecutiveCalls('BEN', '001', '201710'));


        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryMultipleTermId')));

        $response = $this->classroom->getClassrooms();
        $this->assertEquals($this->expectedMultipleTermIdResponse(), $response->getPayload());
    }

    /**
     * Mock Options for Multiple Term Id
     */
    public function mockOptionsMultipleTermId()
    {
        return [
            'termIds' => ['201710', '201720']
        ];
    }

    /**
     * Mock Database Return for Multiple Term Ids
     */
    public function mockQueryMultipleTermId()
    {
        return
            [
                [
                    'ssrmeet_bldg_code' => 'BEN',
                    'ssrmeet_room_code' => '001',
                    'ssrmeet_term_code' => '201710',
                    'ssbsect_subj_code' => 'CSE',
                    'ssbsect_crse_numb' => '148',
                    'ssbsect_crse_title' => null,
                    'ssbsect_crn' => '59387',
                    'ssbsect_camp_code' => 'null',
                    'begin_time' => '11:30',
                    'end_time' => '12:50',
                    'days' => 'MW',
                    'szbuniq_unique_id' => 'MEANEYS2',
                    'szbuniq_pidm' => '1430100',
                    'sirasgn_crn' => '59387'
                ],
                [
                    'ssrmeet_bldg_code' => 'BEN',
                    'ssrmeet_room_code' => '001',
                    'ssrmeet_term_code' => '201720',
                    'ssbsect_subj_code' => 'CSE',
                    'ssbsect_crse_numb' => '102',
                    'ssbsect_crse_title' => null,
                    'ssbsect_crn' => '76317',
                    'ssbsect_camp_code' => 'null',
                    'begin_time' => '12:00',
                    'end_time' => '13:50',
                    'days' => 'F',
                    'szbuniq_unique_id' => 'BRAVOSAD',
                    'szbuniq_pidm' => '1603342',
                    'sirasgn_crn' => '76317'
                ],

            ];
    }

    /**
     * Expected Return from Multiple Term Ids
     */
    public function expectedMultipleTermIdResponse()
    {
        return
            [
                [
                    'buildingCode' => 'BEN',
                    'roomNumber' => '001',
                    'termId' => '201710',
                    'courses' =>
                        [
                            [
                                'subjCode' => 'CSE',
                                'courseNumber' => '148',
                                'courseTitle' => null,
                                'courseCrn' => '59387',
                                'campusCode' => 'null',
                                'startTime' => '11:30',
                                'endTime' => '12:50',
                                'days' => 'MW',
                                'termId' => '201710',
                                'instructors' =>
                                    [
                                        [
                                            'uniqueId' => 'MEANEYS2',
                                            'pidm' => '1430100',
                                            'assignmentCrn' => '59387'
                                        ],
                                    ],
                            ],
                        ],
                ],
                [
                    'buildingCode' => 'BEN',
                    'roomNumber' => '001',
                    'termId' => '201720',
                    'courses' =>
                        [
                            [
                                'subjCode' => 'CSE',
                                'courseNumber' => '102',
                                'courseTitle' => null,
                                'courseCrn' => '76317',
                                'campusCode' => 'null',
                                'startTime' => '12:00',
                                'endTime' => '13:50',
                                'days' => 'F',
                                'termId' => '201720',
                                'instructors' =>
                                    [
                                        [
                                            'uniqueId' => 'BRAVOSAD',
                                            'pidm' => '1603342',
                                            'assignmentCrn' => '76317'
                                        ],
                                    ],
                            ],
                        ],
                ],

            ];
    }
}
